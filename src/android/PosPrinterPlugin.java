package com.cordova;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.content.ServiceConnection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import net.posprinter.service.PosprinterService;
import net.posprinter.service.PosprinterService.MyBinder;

import static android.content.Context.BIND_AUTO_CREATE;

public class PosPrinterPlugin extends CordovaPlugin {

    private static final String TAG = "PosPrinterPlugin";
    private MyBinder service;
    private boolean isBind = false;

    // Used to (un)bind the service to with the activity
    public final ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder _service) {
            // PosprinterService binder = (PosprinterService) service;
            // service = binder.onBind(null);
            service = (MyBinder) _service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "PosPrinterService got disconnected");
        }
    };

    protected void pluginInitialize() {
        Log.i(TAG, "Initializing Printer Service");
        this.startService();
    }

    private Context getCurrentActivity() {
        return cordova.getActivity();
    }

    @Override
    public void onDestroy() {
        synchronized (this) {
            // destroy any components if needed
        }
    }

    /**
     * Bind the activity to a background service and put them into foreground state.
     */
    private void startService() {
        if (!this.isBind) {
            Activity context = cordova.getActivity();
            Intent intent = new Intent(context, PosprinterService.class);
            try {
                context.bindService(intent, this.connection, BIND_AUTO_CREATE);
                context.startService(intent);
            } catch (Exception e) {
                Log.e(TAG, "Issue in starting PosPrinterService", e);
            }
            this.isBind = true;
        }
    }

    /**
     * Bind the activity to a background service and put them into foreground state.
     */
    private void stopService() {
        if (this.isBind) {
            Activity context = cordova.getActivity();
            Intent intent = new Intent(context, PosprinterService.class);

            context.unbindService(this.connection);
            context.stopService(intent);

            this.isBind = false;
        }
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("connect")) {
            String ip = args.getString(0);
            connect(ip, callbackContext);
            return true;
        }
        return false;
    }

    private void connect(String ip, CallbackContext callbackContext) {
        service.connectNetPort(ip, 9100, new net.posprinter.posprinterface.UiExecute() {
            @Override
            public void onsucess() {
                callbackContext.success("connected successfully");
            }

            @Override
            public void onfailed() {
                callbackContext.error("connection failed");
            }
        });
    }

}