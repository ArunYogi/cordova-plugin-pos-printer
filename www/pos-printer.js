module.exports = {
    connect: function (ip, successCallback, failureCallback) {
        cordova.exec(successCallback, failureCallback, "PosPrinterPlugin", "connect", [ip]);
    }
};